#postfix

####Table of Contents

1. [Overview](#overview)
2. [Setup - The basics of getting started with postfix](#setup)
    * [What postfix affects](#what-postfix-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with postfix](#beginning-with-postfix)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)
7. [License - License information](#license)

##Overview

The postfix module installs, configures, and manages the postfix service.

##Setup

###What postfix affects
* postfix package.
* postfix configuration file.
* postfix service.

###Beginning with postfix

```puppet
class { 'postfix':
		relayhost  => 'outgoing-mx.wcw.local',
        mynetworks => '192.168.11.0/24,10.1.25.0/24,127.0.0.0/8',
        hostname   => "${::fqdn}"
}
```

##Usage

All interaction with the postfix module can do be done through the main postfix class.

###I just want postfix, what's the minimum I need?

```puppet
class { 'postfix':
		relayhost  => 'outgoing-mx.wcw.local',
        mynetworks => '192.168.11.0/24,10.1.25.0/24,127.0.0.0/8',
}
```

##Reference

###Classes

####Public Classes

* postfix: Main class, includes all other classes.

####Private Classes

* postfix::package: Handles the Postfix package install.
* postfix::config: Handles the configuration files.
* postfix::service: Handles the service.

###Parameters for postfix

The following parameters are available in the postfix module:

####`hostname`
Defaults to `::fqdn`

####`mynetworks`
There is no default

####`relayhost`
There is no default.

##Limitations

This module has been built on and tested against Puppet Enterprise 3.3 and higher.

The module has been tested on:

* CentOS 6.5

Support on other platforms has not been tested and is currently not supported. 

##Development

###License
Copyright (C) 2014 Mike Travis

For the relevant commits Copyright (C) by the respective authors.

Can be contacted at: mike.r.travis@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
# Class: postfix::config
#
# This module manages the postfix configuration.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# This class is not called directly. 
class postfix::config {

  file { '/etc/postfix/main.cf':
    ensure  => 'file',
    content => template('postfix/main.cf.erb'),
    mode    => '0644',
    notify  => Service['postfix']
  }
 
}
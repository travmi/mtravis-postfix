# Class: postfix::package
#
# This module manages the postfix package.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# This class is not called directly.
class postfix::package {

  package { 'postfix':
    ensure => 'latest',
  }

}
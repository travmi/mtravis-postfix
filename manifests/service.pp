# Class: postfix::service
#
# This module manages the postfix service.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# This class is not called directly.
class postfix::service {
  
  service { 'postfix':
    ensure => 'running',
    enable => true,
  }
    
}
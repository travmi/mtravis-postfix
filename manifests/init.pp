# == Class: postfix
#
# This module manages installation, configuration and postfix service.
#
# === Parameters
#
# Document parameters here.
#
# [*hostname*]
# This defaults to the FQDN of your server. 
#
# [*mynetworks*]
# Define which networks postfix is able to send on.
#
# [*relayhost*]
# Define the main mail server for your company.
#
# [*whitelist_enable*]
# If this is set to true then the whitelist will be enabled on the server.
# Defaults to false.
#
# === Examples
#
#  class { postfix:
#    hostname   => foobar.test.local
#    mynetworks => [10.1.25.0/24,192.168.11.0/24,127.0.0.0/8]
#    relayhost  => outgoing-mx.wcw.local
#  }
class postfix (
  
  $hostname         = "${::fqdn}",
  $mynetworks       = undef,
  $relayhost        = undef,
  $whitelist_enable = false,
  $url              = undef,
  
){

  validate_string($hostname)
  validate_string($relayhost)
  validate_bool($whitelist_enable)

  if $whitelist_enable {
    file { '/etc/postfix/whitelist_recipient':
      ensure => 'file',
      source => "puppet:///modules/postfix/whitelist_recipient",
      mode   => '0644',
      notify => Service['postfix']
    }
  }

 anchor { 'postfix::begin': } ->
    class  { '::postfix::package': } ->
    class  { '::postfix::config': } ~>
    class  { '::postfix::service': } ->
 anchor { 'postfix::end': }

}
